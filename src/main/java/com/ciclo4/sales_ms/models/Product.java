package com.ciclo4.sales_ms.models;

import org.springframework.data.annotation.Id;



public class Product {

    @Id
    private String product;
    /* valor sugerido por unidad */
    private Integer value1;
    /* valor sugerido por docena */
    private Integer value2;
    /*valor sugerido superior docena*/
    private Integer value3;


    public Product( String product, Integer value1, Integer value2, Integer value3){

        this.product = product;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;


    }



    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getValue1() {
        return value1;
    }

    public void setValue1(Integer value1) {
        this.value1 = value1;
    }

    public Integer getValue2() {
        return value2;
    }

    public void setValue2(Integer value2) {
        this.value2 = value2;
    }

    public Integer getValue3() {
        return value3;
    }

    public void setValue3(Integer value3) {
        this.value3 = value3;
    }


}
