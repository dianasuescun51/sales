package com.ciclo4.sales_ms.models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Sales {
    @Id
    private String invoice;
    private Date saleDate;
    private String client;
    private String city;
    private String saleProduct;
    private Integer quantity;
    private Integer salePrice;
    private Integer total;
    private String paymentMethod;
    private Integer prePayment;
    private Integer balance;

    public Sales(Date saleDate, String client, String city, String invoice, String saleProduct, Integer quantity,
                   Integer salePrice, Integer total, String paymentMethod, Integer prePayment, Integer balance){

        this.saleDate = saleDate;
        this.client = client;
        this.city = city;
        this.invoice = invoice;
        this.saleProduct = saleProduct;
        this.quantity = quantity;
        this.salePrice = salePrice;
        this.total = total;
        this.paymentMethod = paymentMethod;
        this.prePayment = prePayment;
        this.balance = balance;

    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getSaleProduct() {
        return saleProduct;
    }

    public void setSaleProduct(String saleProduct) {
        this.saleProduct = saleProduct;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getPrePayment() {
        return prePayment;
    }

    public void setPrePayment(Integer prePayment) {
        this.prePayment = prePayment;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
