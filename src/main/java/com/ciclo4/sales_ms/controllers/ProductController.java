package com.ciclo4.sales_ms.controllers;

import com.ciclo4.sales_ms.exceptions.ProductNotFoundException;
import com.ciclo4.sales_ms.models.Product;
import com.ciclo4.sales_ms.repositories.ProductRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication
@RestController

public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;

    }

        @PostMapping("/product")
    Product newProduct(@RequestBody Product product){
        return productRepository.save(product);
    }

    @DeleteMapping("/product/{item}")
    String deleteProduct(@PathVariable String item){
        productRepository.deleteById(item);
        return ("Producto borrado exitosamente");

    }

    @GetMapping("/products/{product}")
    Product getProduct(@PathVariable String product){
        return productRepository.findById(product).orElseThrow(()->new ProductNotFoundException("No se encontró el producto"+ product));
    }

    @GetMapping("/products")
    public List listAllProducts() {
      return productRepository.findAll();
    }





}
