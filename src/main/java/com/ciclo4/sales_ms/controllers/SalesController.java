package com.ciclo4.sales_ms.controllers;


import com.ciclo4.sales_ms.exceptions.ProductNotFoundException;
import com.ciclo4.sales_ms.exceptions.SalesNotFoundException;
import com.ciclo4.sales_ms.models.Product;
import com.ciclo4.sales_ms.models.Sales;
import com.ciclo4.sales_ms.repositories.ProductRepository;
import com.ciclo4.sales_ms.repositories.SalesRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication
@RestController
public class SalesController {
    private final SalesRepository salesRepository;
    private final ProductRepository productRepository;

    public SalesController(SalesRepository salesRepository, ProductRepository productRepository) {
        this.salesRepository = salesRepository;
        this.productRepository = productRepository;
    }

    @PostMapping("/sale")
    Sales newSale(@RequestBody Sales sales){
        Product itemOrigin = productRepository.findById(sales.getSaleProduct()).orElseThrow(null);

        if (itemOrigin == null){
          throw new ProductNotFoundException("No existe un producto con el nombre " + sales.getSaleProduct());
        }

        return salesRepository.save(sales);
    }

    @DeleteMapping("/sale/{invoice}")
    String deleteProduct(@PathVariable String invoice){
        salesRepository.deleteById(invoice);
        return ("Venta borrada exitosamente");

    }

    @GetMapping("/sale/{invoice}")
    Sales getSale(@PathVariable String invoice){
        return salesRepository.findById(invoice).orElseThrow(()->new SalesNotFoundException("No se encontró la factura"+ invoice));
    }

    @GetMapping("/sales")
    public List listAllProducts() {
        return salesRepository.findAll();
    }



    }
