package com.ciclo4.sales_ms.repositories;

import com.ciclo4.sales_ms.models.Sales;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SalesRepository extends MongoRepository <Sales, String> {

}
