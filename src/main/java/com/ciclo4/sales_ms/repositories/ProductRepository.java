package com.ciclo4.sales_ms.repositories;

import com.ciclo4.sales_ms.models.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository <Product, String> {

}
