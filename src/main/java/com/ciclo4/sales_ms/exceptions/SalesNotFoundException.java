package com.ciclo4.sales_ms.exceptions;

public class SalesNotFoundException extends RuntimeException {

    public SalesNotFoundException (String message){
        super(message);
    }

}
