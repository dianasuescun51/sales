package com.ciclo4.sales_ms.exceptions;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException (String message){
        super(message);
    }

}
